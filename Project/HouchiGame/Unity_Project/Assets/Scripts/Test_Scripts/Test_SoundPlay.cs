﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Test_SoundPlay : Base
{
	void Update()
	{
		//サウンドテスト用
		if( Input.GetKeyDown( KeyCode.Q ) ) { csBase_Sound.BaseFnc_SoundPlay( Tbl_SoundTable.E_SND_TAG.E_SND_1000_SE_1 ); }
		if( Input.GetKeyDown( KeyCode.W ) ) { csBase_Sound.BaseFnc_SoundPlay( Tbl_SoundTable.E_SND_TAG.E_SND_1001_SE_2 ); }
		if( Input.GetKeyDown( KeyCode.E ) ) { csBase_Sound.BaseFnc_SoundPlay( Tbl_SoundTable.E_SND_TAG.E_SND_1100_SE_3 ); }
		if( Input.GetKeyDown( KeyCode.R ) ) { csBase_Sound.BaseFnc_SoundPlay( Tbl_SoundTable.E_SND_TAG.E_SND_1200_SE_4 ); }
		if( Input.GetKeyDown( KeyCode.A ) ) { csBase_Sound.BaseFnc_SoundPlay( Tbl_SoundTable.E_SND_TAG.E_SND_2000_VO_1 ); }
		if( Input.GetKeyDown( KeyCode.S ) ) { csBase_Sound.BaseFnc_SoundPlay( Tbl_SoundTable.E_SND_TAG.E_SND_2001_VO_2 ); }
		if( Input.GetKeyDown( KeyCode.D ) ) { csBase_Sound.BaseFnc_SoundPlay( Tbl_SoundTable.E_SND_TAG.E_SND_2100_VO_3 ); }
		if( Input.GetKeyDown( KeyCode.F ) ) { csBase_Sound.BaseFnc_SoundPlay( Tbl_SoundTable.E_SND_TAG.E_SND_2101_VO_4 ); }
		if( Input.GetKeyDown( KeyCode.Z ) ) { csBase_Sound.BaseFnc_SoundPlay( Tbl_SoundTable.E_SND_TAG.E_SND_3000_BGM_1 ); }
		if( Input.GetKeyDown( KeyCode.X ) ) { csBase_Sound.BaseFnc_SoundPlay( Tbl_SoundTable.E_SND_TAG.E_SND_3001_BGM_2 ); }
		if( Input.GetKeyDown( KeyCode.C ) ) { csBase_Sound.BaseFnc_SoundPlay( Tbl_SoundTable.E_SND_TAG.E_SND_3100_BGM_3 ); }
		if( Input.GetKeyDown( KeyCode.V ) ) { csBase_Sound.BaseFnc_SoundPlay( Tbl_SoundTable.E_SND_TAG.E_SND_3101_BGM_4 ); }
	}
}
