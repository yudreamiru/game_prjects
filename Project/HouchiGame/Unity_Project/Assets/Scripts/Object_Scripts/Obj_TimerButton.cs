﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obj_TimerButton : MonoBehaviour
{
	//モードに対応するボタンリスト
	public GameObject[] goButtonObjList = new GameObject[3];

	private void Reset()
	{
	}

	private void Start()
	{
		//代入チェック
		Base_DataCheck.BaseFnc_NullCheck( goButtonObjList[( int )Man_Time.E_TIMER_MODE.TIMER_MODE_TIMER]		);
		Base_DataCheck.BaseFnc_NullCheck( goButtonObjList[( int )Man_Time.E_TIMER_MODE.TIMER_MODE_STOPWATCH]	);
		Base_DataCheck.BaseFnc_NullCheck( goButtonObjList[( int )Man_Time.E_TIMER_MODE.TIMER_MODE_CLOCK]		);

		Reset();
	}

	public void Objfunc_UpdateButtonObjList( Man_Time.E_TIMER_MODE eArg ) {

		switch( eArg ) {
			case Man_Time.E_TIMER_MODE.TIMER_MODE_TIMER:
				goButtonObjList[( int )Man_Time.E_TIMER_MODE.TIMER_MODE_TIMER]		.SetActive( true );
				goButtonObjList[( int )Man_Time.E_TIMER_MODE.TIMER_MODE_STOPWATCH]	.SetActive( false );
				goButtonObjList[( int )Man_Time.E_TIMER_MODE.TIMER_MODE_CLOCK]		.SetActive( false );
				break;

			case Man_Time.E_TIMER_MODE.TIMER_MODE_STOPWATCH:
				goButtonObjList[( int )Man_Time.E_TIMER_MODE.TIMER_MODE_TIMER].SetActive( false );
				goButtonObjList[( int )Man_Time.E_TIMER_MODE.TIMER_MODE_STOPWATCH].SetActive( true );
				goButtonObjList[( int )Man_Time.E_TIMER_MODE.TIMER_MODE_CLOCK].SetActive( false );
				break;

			case Man_Time.E_TIMER_MODE.TIMER_MODE_CLOCK:
				goButtonObjList[( int )Man_Time.E_TIMER_MODE.TIMER_MODE_TIMER].SetActive( false );
				goButtonObjList[( int )Man_Time.E_TIMER_MODE.TIMER_MODE_STOPWATCH].SetActive( false );
				goButtonObjList[( int )Man_Time.E_TIMER_MODE.TIMER_MODE_CLOCK].SetActive( true );
				break;

			default:
				Debug_Assert.DebugFnc_Assert( eArg );
				break;
		}
	}
}
