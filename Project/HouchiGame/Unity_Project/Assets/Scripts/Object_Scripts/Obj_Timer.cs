﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Obj_Timer : MonoBehaviour
{
	public SpriteRenderer[] spSpriteRendererList = new SpriteRenderer[6];
	public Sprite[] spSpriteList = new Sprite[10];

	private void Reset()
	{
	}

	private void Start()
	{
		//代入チェック
		Base_DataCheck.BaseFnc_NullCheck( spSpriteRendererList[0] );
		Base_DataCheck.BaseFnc_NullCheck( spSpriteRendererList[1] );
		Base_DataCheck.BaseFnc_NullCheck( spSpriteRendererList[2] );
		Base_DataCheck.BaseFnc_NullCheck( spSpriteRendererList[3] );
		Base_DataCheck.BaseFnc_NullCheck( spSpriteRendererList[4] );
		Base_DataCheck.BaseFnc_NullCheck( spSpriteRendererList[5] );

		Reset();
	}

	private void Update()
	{
	}

	//----------------------------------------------------------------
	// Detail  ；タイマーオブジェクトの画像の更新
	// Argument：TimeSpan:経過時間
	// Return  ：none
	// Create  ：takada
	//----------------------------------------------------------------
	public void Objfunc_UpdateTimerObjSprite( TimeSpan argTime )
	{
		//画像の更新
		spSpriteRendererList[0].sprite = spSpriteList[argTime.Hours / 10];
		spSpriteRendererList[1].sprite = spSpriteList[argTime.Hours % 10];
		spSpriteRendererList[2].sprite = spSpriteList[argTime.Minutes / 10];
		spSpriteRendererList[3].sprite = spSpriteList[argTime.Minutes % 10];
		spSpriteRendererList[4].sprite = spSpriteList[argTime.Seconds / 10];
		spSpriteRendererList[5].sprite = spSpriteList[argTime.Seconds % 10];
	}
	public void Objfunc_UpdateTimerObjSprite( DateTime argTime )
	{
		//画像の更新
		spSpriteRendererList[0].sprite = spSpriteList[argTime.Hour / 10];
		spSpriteRendererList[1].sprite = spSpriteList[argTime.Hour % 10];
		spSpriteRendererList[2].sprite = spSpriteList[argTime.Minute / 10];
		spSpriteRendererList[3].sprite = spSpriteList[argTime.Minute % 10];
		spSpriteRendererList[4].sprite = spSpriteList[argTime.Second / 10];
		spSpriteRendererList[5].sprite = spSpriteList[argTime.Second % 10];
	}
	public void Objfunc_UpdateTimerObjSprite()
	{
		//画像の更新
		spSpriteRendererList[0].sprite = spSpriteList[0];
		spSpriteRendererList[1].sprite = spSpriteList[0];
		spSpriteRendererList[2].sprite = spSpriteList[0];
		spSpriteRendererList[3].sprite = spSpriteList[0];
		spSpriteRendererList[4].sprite = spSpriteList[0];
		spSpriteRendererList[5].sprite = spSpriteList[0];
	}
}
