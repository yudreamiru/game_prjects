﻿using UnityEngine;
using System;

public class Sys_Timer : MonoBehaviour
{
    public Obj_Timer csObj_Timer;           //タイマー機能管理
    //private bool bTimerCountFlag;         //タイマー動作フラグ
    private enum E_TIMER_MODE                  // タイマーのモードの種類
    {
        TIMER_MODE_STOP,
        TIMER_MODE_START,
        TIMER_MODE_END,
        TIMER_MODE_PAUSE,
        TIMER_MODE_LEGTH
    }
    E_TIMER_MODE eTimerMode = E_TIMER_MODE.TIMER_MODE_STOP;  // タイマーのモード
    //private DateTime daStartTime;         //計測開始時間
    private int iSetSecond = 60;            //設定時間(秒)
    public DateTime daPausedTime;           // 一時停止開始時間
    public DateTime endTime;                // 終了時間

    //----------------------------------------------------------------
    // Detail  ：タイマー使用前の事前更新
    // Argument：none
    // Return  ：none
    // Create  ：takada
    //----------------------------------------------------------------
    public void SysFunc_Reset()
    {
        eTimerMode = E_TIMER_MODE.TIMER_MODE_STOP;
        csObj_Timer.Objfunc_UpdateTimerObjSprite(new TimeSpan(0, 0, 0, iSetSecond, 0));
        daPausedTime = Base_Time.BaseFnc_Get_DateTime();
    }

    void Start()
    {
        Base_DataCheck.BaseFnc_NullCheck(csObj_Timer);　//代入チェック
    }

    //----------------------------------------------------------------
    // Detail  ：アップデート
    // Argument：none
    // Return  ：none
    // Create  ：takada
    //----------------------------------------------------------------
    public void SysFnc_Update()
    {
        // タイマーモードに応じてアップデート内容を変更
        switch(eTimerMode)
        {
            case E_TIMER_MODE.TIMER_MODE_START:   // スタート
                // タイマーがゼロになった時 END モードに変更
                if (endTime - Base_Time.BaseFnc_Get_DateTime() < new TimeSpan(0, 0, 0, 0, 0))
                {
                    eTimerMode = E_TIMER_MODE.TIMER_MODE_END;
                    csObj_Timer.Objfunc_UpdateTimerObjSprite(new TimeSpan(0, 0, 0, 0, 0));
                }
                // 制限時間内画像の更新
                else csObj_Timer.Objfunc_UpdateTimerObjSprite(endTime - Base_Time.BaseFnc_Get_DateTime());
                break;

            case E_TIMER_MODE.TIMER_MODE_END:     // エンド(制限時間切れ)
                // DEBUG
                if (Input.anyKeyDown)
                {
                    // ボタンが押されたとき STOP モードに変更
                    eTimerMode = E_TIMER_MODE.TIMER_MODE_STOP;
                    // DEBUG 時間設定
                    SysFunc_TimerSetNumKeyTrigger();
                    SysFunc_TimerSetArrowKeyTrigger();
                }
                break;

            case E_TIMER_MODE.TIMER_MODE_STOP:
                // DEBUG 時間設定
                if (Input.anyKeyDown)
                {
                    SysFunc_TimerSetNumKeyTrigger();
                    SysFunc_TimerSetArrowKeyTrigger();
                }

                break;

            default:
                break;
        }
    }

    //----------------------------------------------------------------
    // Detail  ；タイマースタート
    // Argument：none
    // Return  ：none
    // Create  ：nariba
    //----------------------------------------------------------------
    public void SysFunc_TimerStart()
    {
        if (eTimerMode != E_TIMER_MODE.TIMER_MODE_START)
        {
            if(eTimerMode == E_TIMER_MODE.TIMER_MODE_PAUSE)
            {
                // 終了予定時刻からポーズ中に経過してた時間を加算
                endTime += new TimeSpan(
                    Base_Time.BaseFnc_Get_DateTime().Day,
                    Base_Time.BaseFnc_Get_DateTime().Hour,
                    Base_Time.BaseFnc_Get_DateTime().Minute,
                    Base_Time.BaseFnc_Get_DateTime().Second,
                    Base_Time.BaseFnc_Get_DateTime().Millisecond) -
                new TimeSpan(
                    daPausedTime.Day,
                    daPausedTime.Hour,
                    daPausedTime.Minute,
                    daPausedTime.Second,
                    daPausedTime.Millisecond);
            }
            else
            {
                // ポーズ以外は通常終了予定時刻に設定
                endTime = Base_Time.BaseFnc_Get_DateTime() + new TimeSpan(0, 0, 0, iSetSecond, 0);
            }
            eTimerMode = E_TIMER_MODE.TIMER_MODE_START;
        }
    }

    //----------------------------------------------------------------
    // Detail  ；タイマーポーズ
    // Argument：none
    // Return  ：none
    // Create  ：nariba
    //----------------------------------------------------------------
    public void SysFunc_TimerPaused()
    {
        if(eTimerMode != E_TIMER_MODE.TIMER_MODE_PAUSE)
        {
            eTimerMode = E_TIMER_MODE.TIMER_MODE_PAUSE;
            daPausedTime = Base_Time.BaseFnc_Get_DateTime();
        }
        
    }

    //----------------------------------------------------------------
    // Detail  ；タイマー設定時間変更
    // Argument：int _iSetSecond 設定したい秒
    // Return  ：none
    // Create  ：nariba
    //----------------------------------------------------------------
    void SysFunc_TimerUpdateSetSecond(int _iSetSecond)
    {
        iSetSecond = _iSetSecond;
        if (iSetSecond <= 0) iSetSecond = 1;
        if (iSetSecond >= 60 * 60 * 24) iSetSecond = 86399;
        SysFunc_Reset();
    }

    //----------------------------------------------------------------
    // Detail  ；タイマー設定時間加算
    // Argument：int _iAddMinutes 増やす秒数
    // Return  ：none
    // Create  ：nariba
    //----------------------------------------------------------------
    public void SysFunc_TimerAddSecond(int _iAddSecond)
    {
        if(eTimerMode == E_TIMER_MODE.TIMER_MODE_STOP)
        {
            SysFunc_TimerUpdateSetSecond(iSetSecond + _iAddSecond);
        }
    }

    //----------------------------------------------------------------
    // Detail  ；タイマー設定時間減算
    // Argument：int _iMinusMinutes 減らす秒数
    // Return  ：none
    // Create  ：nariba
    //----------------------------------------------------------------
    public void SysFunc_TimerMinusSecond(int _iMinusSecond)
    {
        if (eTimerMode == E_TIMER_MODE.TIMER_MODE_STOP)
        {
            SysFunc_TimerUpdateSetSecond(iSetSecond - _iMinusSecond);
        }
    }

    //----------------------------------------------------------------
    // Detail  ；タイマー設定するボタンの判定
    // Argument：int num 指定した数字のキー判定
    // Return  ：none
    // Create  ：nariba
    //----------------------------------------------------------------
    private void SysFunc_TimerSetNumKeyTrigger(int num = -1)
    {
        // 何かのキーが押下されたら
        if (Input.anyKeyDown)
        {
            foreach (KeyCode code in Enum.GetValues(typeof(KeyCode)))
            {
                if (Input.GetKeyDown(code))
                {
                    if (num == -1)
                    {
                        for (int i = 0; i < 10; i++)
                        {
                            if (code.ToString() == "Alpha" + i.ToString())
                            {
                                if (i == 0) SysFunc_TimerUpdateSetSecond(30);
                                else SysFunc_TimerUpdateSetSecond(i * 60);
                                break;
                            }
                        }
                    }
                    else
                    {
                        if (code.ToString() == "Alpha" + num.ToString())
                        {
                            if (num == 0) SysFunc_TimerUpdateSetSecond(30);
                            SysFunc_TimerUpdateSetSecond(num * 60);
                            break;
                        }
                    }
                    
                }
            }
        }
    }

    //----------------------------------------------------------------
    // Detail  ；タイマー設定する十字キーの判定
    // Argument：none
    // Return  ：none
    // Create  ：nariba
    //----------------------------------------------------------------
    private void SysFunc_TimerSetArrowKeyTrigger()
    {
        if (Input.GetKeyDown(KeyCode.UpArrow)) SysFunc_TimerAddSecond(60);
        if (Input.GetKeyDown(KeyCode.DownArrow)) SysFunc_TimerMinusSecond(60);
    }
}
