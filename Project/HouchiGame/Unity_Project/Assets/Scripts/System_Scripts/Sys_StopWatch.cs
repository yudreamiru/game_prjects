﻿using UnityEngine;
using System;

public class Sys_StopWatch : MonoBehaviour
{

	public Obj_Timer csObj_Timer;       //タイマー機能管理

	//private bool bTimerCountFlag;       //タイマー動作フラグ

	private DateTime daStartTime;       //計測開始時間
	private DateTime daPausedTime;		// ポーズ時間

	public enum E_STW_MODE
    {
		STW_MODE_STOP,
		STW_MODE_START,
		STW_MODE_PAUSE,
		STW_MODE_LENGTH
    }

	public E_STW_MODE eSTWMode = E_STW_MODE.STW_MODE_STOP;

	public void SysFunc_Reset()
	{
		//計測開始時間のリセット
		daStartTime = Base_Time.BaseFnc_Get_DateTime();
	}

	void Start()
	{
		SysFunc_Reset();
	}

	public void SysFnc_Update()
	{
		switch(eSTWMode)
        {
			//経過時間の更新
			case E_STW_MODE.STW_MODE_START:
				csObj_Timer.Objfunc_UpdateTimerObjSprite(new TimeSpan(
					Base_Time.BaseFnc_Get_DateTime().Day,
					Base_Time.BaseFnc_Get_DateTime().Hour,
					Base_Time.BaseFnc_Get_DateTime().Minute,
					Base_Time.BaseFnc_Get_DateTime().Second,
					Base_Time.BaseFnc_Get_DateTime().Millisecond) -
				new TimeSpan(
					daStartTime.Day,
					daStartTime.Hour,
					daStartTime.Minute,
					daStartTime.Second,
					daStartTime.Millisecond));
				break;

			default:
				break;
		}
	}

	//----------------------------------------------------------------
	// Detail  ；ストップウォッチリセット
	// Argument：none
	// Return  ：none
	// Create  ：takada
	//----------------------------------------------------------------
	public void SysFunc_STWReset()
	{
		//計測開始時間のリセット
		daStartTime = Base_Time.BaseFnc_Get_DateTime();
		csObj_Timer.Objfunc_UpdateTimerObjSprite();

		eSTWMode = E_STW_MODE.STW_MODE_STOP;
	}

	//----------------------------------------------------------------
	// Detail  ；ストップウォッチスタート
	// Argument：none
	// Return  ：none
	// Create  ：takada
	//----------------------------------------------------------------
	public void SysFunc_STWStart()
	{
		if(eSTWMode != E_STW_MODE.STW_MODE_START)
        {
            switch (eSTWMode)
            {
                case E_STW_MODE.STW_MODE_STOP:
					daStartTime = Base_Time.BaseFnc_Get_DateTime();
					break;
                case E_STW_MODE.STW_MODE_PAUSE:
					daStartTime += new TimeSpan(
					Base_Time.BaseFnc_Get_DateTime().Day,
					Base_Time.BaseFnc_Get_DateTime().Hour,
					Base_Time.BaseFnc_Get_DateTime().Minute,
					Base_Time.BaseFnc_Get_DateTime().Second,
					Base_Time.BaseFnc_Get_DateTime().Millisecond) -
				new TimeSpan(
					daPausedTime.Day,
					daPausedTime.Hour,
					daPausedTime.Minute,
					daPausedTime.Second,
					daPausedTime.Millisecond);
					break;

				default:
					break;

			}

			eSTWMode = E_STW_MODE.STW_MODE_START;
		}
	}

	//----------------------------------------------------------------
	// Detail  ；ストップウォッチポーズ
	// Argument：none
	// Return  ：none
	// Create  ：takada
	//----------------------------------------------------------------
	public void SysFunc_STWPause()
	{
		if(eSTWMode != E_STW_MODE.STW_MODE_PAUSE)
        {
			// 
			daPausedTime = Base_Time.BaseFnc_Get_DateTime();
			eSTWMode = E_STW_MODE.STW_MODE_PAUSE;
		}
	}
}
