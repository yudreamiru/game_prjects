﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Sys_Clock : MonoBehaviour
{

	public Obj_Timer csObj_Timer;       //タイマー機能管理

	public void SysFunc_Reset()
	{
		csObj_Timer.Objfunc_UpdateTimerObjSprite( );
	}

	void Start()
    {
		SysFunc_Reset();

	}

    public void SysFnc_Update()
    {
		csObj_Timer.Objfunc_UpdateTimerObjSprite( Base_Time.BaseFnc_Get_DateTime() );
	}
}
