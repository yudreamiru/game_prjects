﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Base : MonoBehaviour
{
	public Base_Sound csBase_Sound;
    
	
    void Start()
    {
		//サウンドプレイヤー
		csBase_Sound = BaseFnc_GetGameObject( "Base_SoundPlayer" ).GetComponent< Base_Sound >();
		Base_DataCheck.BaseFnc_NullCheck( csBase_Sound );

	}

    void Update()
    {
        
    }

	//----------------------------------------------------------------
	// Detail  ；オブジェクト検索
	// Argument：検索オブジェクト名
	// Return  ：検索結果
	// Create  ：takada
	//----------------------------------------------------------------
	private GameObject BaseFnc_GetGameObject(string stObjName)
	{
		GameObject goGetGameObject = null;  //取得用
		goGetGameObject = GameObject.Find( stObjName );
		Base_DataCheck.BaseFnc_NullCheck( stObjName );
		return goGetGameObject;
	}
}
