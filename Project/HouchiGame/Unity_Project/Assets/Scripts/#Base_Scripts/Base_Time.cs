﻿using System;

public static class Base_Time
{
	
	//指定時間から現在時間までの経過時間を取得
	public static TimeSpan BaseFnc_Get_Keika_Time( DateTime argTime )
	{
		TimeSpan retTime = BaseFnc_Get_DateTime() - argTime;
		return retTime;
	}

	//指定時間から現在時間までの経過時間を秒で取得
	public static int BaseFnc_Get_Keika_Min( DateTime argTime )
	{
		TimeSpan baseTime = BaseFnc_Get_Keika_Time( argTime );
		int uiRetSec =
			( baseTime.Days * 24 * 60 * 60
			+ baseTime.Hours * 60 * 60
			+ baseTime.Minutes * 60
			+ baseTime.Seconds);

		return uiRetSec;
	}

	//経過時間を秒に変換
	public static int BaseFnc_Get_Keika_Min( TimeSpan argTime )
	{
		int uiRetSec =
			( argTime.Days * 24 * 60 * 60
			+ argTime.Hours * 60 * 60
			+ argTime.Minutes * 60
			+ argTime.Seconds);

		return uiRetSec;
	}

	//現在の時間情報を取得 
	public static DateTime BaseFnc_Get_DateTime(){DateTime dateTime = DateTime.UtcNow;return dateTime.AddHours(9);}
	//現在の年を取得 
	public static int BaseFnc_Get_Year(){DateTime dateTime = DateTime.UtcNow;return dateTime.Year;}
	//現在の月を取得 
	public static int BaseFnc_Get_Mon()	{DateTime dateTime = DateTime.UtcNow;return dateTime.Month;}
	//現在の日を取得 
	public static int BaseFnc_Get_Day()	{DateTime dateTime = DateTime.UtcNow;return dateTime.Day;}
	//現在の時を取得 
	public static int BaseFnc_Get_Hour(){DateTime dateTime = DateTime.UtcNow;return dateTime.Hour;}
	//現在の分を取得 
	public static int BaseFnc_Get_Min()	{DateTime dateTime = DateTime.UtcNow;return dateTime.Minute;}
	//現在の秒を取得 
	public static int BaseFnc_Get_Sec()	{DateTime dateTime = DateTime.UtcNow;return dateTime.Second;}
	//現在のミリ秒を取得 
	public static int BaseFnc_Get_MSec() { DateTime dateTime = DateTime.UtcNow; return dateTime.Millisecond; }
}
