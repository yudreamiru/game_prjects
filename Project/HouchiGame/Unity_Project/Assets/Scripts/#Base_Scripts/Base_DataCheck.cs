﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Base_DataCheck
{
	//----------------------------------------------------------------
	// Detail  ；範囲チェック
	// Argument：値/最小値/最大値
	// Return  ：補正値
	// Create  ：takada
	//----------------------------------------------------------------
	public static float BaseFnc_RangeCheck( float arg_fValue, float arg_fMin, float arg_fMax )
	{
		float fRetValue = arg_fValue;
		if( arg_fMin <= arg_fValue && arg_fValue <= arg_fMax )
		{
			//正常値
		}
		else
		{
			//異常値
			//補正
			fRetValue = Mathf.Clamp( arg_fValue, arg_fMin, arg_fMax );

			Debug_Assert.DebugFnc_Assert( false, arg_fValue, arg_fMin, arg_fMax );
		}

		return fRetValue;
	}
	public static float BaseFnc_RangeCheck( float arg_fValue, float arg_fMax )
	{
		float fRetValue = arg_fValue;
		if( 0.0f <= arg_fValue && arg_fValue <= arg_fMax )
		{
			//正常値
		}
		else
		{
			//異常値
			//補正
			fRetValue = Mathf.Clamp( arg_fValue, 0.0f, arg_fMax );

			Debug_Assert.DebugFnc_Assert( false, arg_fValue, 0.0f, arg_fMax );
		}

		return fRetValue;
	}

	//----------------------------------------------------------------
	// Detail  ；NULLチェック
	// Argument：値
	// Return  ：none
	// Create  ：takada
	//----------------------------------------------------------------
	public static void BaseFnc_NullCheck( object arg_oArg )
	{
		if( arg_oArg != null )
		{
			//正常値
		}
		else
		{
			//異常値
			Debug_Assert.DebugFnc_Assert( false, "NULL" );
		}
	}
}
