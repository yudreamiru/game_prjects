﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Base_Sound : Tbl_SoundTable
{

	//サウンドデータ配列
	public AudioClip[] acSndList = new AudioClip[(int)E_SND_TAG.E_SND_LIST_DATA_TAG_LENGTH];

	//頭出し防止用サウンドCH保持配列
	private E_SND_TAG[] eKeepSndCh = new E_SND_TAG[(int)E_SND_CH.E_SND_CH_LENGTH];

	//サウンドCH対応オーディオソース
	private AudioSource[] auSndChList = new AudioSource[(int)E_SND_CH.E_SND_CH_LENGTH];


	private void Reset()
	{
		for( int i = 0;i < auSndChList.Length;i++ )
		{
			//保持用サウンドチャンネルの初期化
			eKeepSndCh[i] = E_SND_TAG.E_SND_LIST_DATA_TAG_LENGTH;
			Base_DataCheck.BaseFnc_NullCheck( eKeepSndCh[i] );
		}
	}

	private void Start()
	{
		Reset();

		for( int i = 0;i < auSndChList.Length;i++ )
		{
			//オーディオソースの生成
			auSndChList[i] = gameObject.AddComponent<AudioSource>();
			Base_DataCheck.BaseFnc_NullCheck( auSndChList[i] );
			auSndChList[i].loop = false;
			auSndChList[i].playOnAwake = false;
		}

		for( int i = 0;i < acSndList.Length;i++ )
		{
			//サウンドデータの登録
			string stDataPash = T_SND_DATA_LIST[i].cmGetFilePash();
			acSndList[i] = Resources.Load<AudioClip>( stDataPash );
			Base_DataCheck.BaseFnc_NullCheck( acSndList[i] );
		}
	}

	private void Update()
	{
		//停止後サウンドのリセット
		for( int i = 0;i < auSndChList.Length;i++ )
		{
			if( eKeepSndCh[i] == E_SND_TAG.E_SND_LIST_DATA_TAG_LENGTH && !auSndChList[i].isPlaying )
			{
				BaseFnc_SoundStop( ( Tbl_SoundTable.E_SND_CH )i );
			}
		}
	}

	//----------------------------------------------------------------
	// Detail  ；サウンド再生
	// Argument：値
	// Return  ：none
	// Create  ：takada
	//----------------------------------------------------------------
	public void BaseFnc_SoundPlay( Tbl_SoundTable.E_SND_TAG eSoundTag )
	{
		//データチェック
		Base_DataCheck.BaseFnc_RangeCheck( ( float )eSoundTag, ( float )Tbl_SoundTable.E_SND_TAG.E_SND_LIST_DATA_TAG_LENGTH );

		//サウンドの定義クラス
		cSndListStat cSndListStat = T_SND_DATA_LIST[(int)eSoundTag];

		//データチェック
		Base_DataCheck.BaseFnc_NullCheck( cSndListStat.cmGetChannel() );
		Base_DataCheck.BaseFnc_NullCheck( cSndListStat.cmGetVolume() );
		Base_DataCheck.BaseFnc_NullCheck( cSndListStat.cmGetLoop() );
		Base_DataCheck.BaseFnc_NullCheck( cSndListStat.cmGetKeep() );
		Base_DataCheck.BaseFnc_RangeCheck( cSndListStat.cmGetChannel(), ( float )Tbl_SoundTable.E_SND_CH.E_SND_CH_LENGTH );

		//保持中の場合処理なし
		if( eKeepSndCh[( int )cSndListStat.cmGetChannel()] == eSoundTag )
		{
			//処理なし
			return;
		}

		//初期化
		auSndChList[cSndListStat.cmGetChannel()].loop = false;
		auSndChList[cSndListStat.cmGetChannel()].volume = 0;

		//ループを有効化
		if( cSndListStat.cmGetLoop() )
		{
			auSndChList[cSndListStat.cmGetChannel()].loop = true;
		}

		//保持を有効化
		if( cSndListStat.cmGetKeep() )
		{
			eKeepSndCh[( int )cSndListStat.cmGetChannel()] = eSoundTag;
		}

		//ボリュームをセット
		auSndChList[cSndListStat.cmGetChannel()].volume = ( float )cSndListStat.cmGetVolume() * 0.01f;

		//オーディオクリップをセット
		auSndChList[cSndListStat.cmGetChannel()].clip = acSndList[( int )eSoundTag];

		//サウンド再生
		auSndChList[cSndListStat.cmGetChannel()].Play();
	}

	//----------------------------------------------------------------
	// Detail  ；サウンド停止
	// Argument：値
	// Return  ：none
	// Create  ：takada
	//----------------------------------------------------------------
	public void BaseFnc_SoundStop( Tbl_SoundTable.E_SND_CH eSoundCh )
	{
		Base_DataCheck.BaseFnc_RangeCheck( ( float )eSoundCh, ( float )Tbl_SoundTable.E_SND_CH.E_SND_CH_LENGTH );

		//保持のリセット
		eKeepSndCh[( int )eSoundCh] = E_SND_TAG.E_SND_LIST_DATA_TAG_LENGTH;
		//サウンドの停止
		auSndChList[( int )eSoundCh].Stop();
	}

	//----------------------------------------------------------------
	// Detail  ；サウンド一時停止
	// Argument：値
	// Return  ：none
	// Create  ：takada
	//----------------------------------------------------------------
	public void BaseFnc_SoundPause( Tbl_SoundTable.E_SND_CH eSoundCh )
	{
		Base_DataCheck.BaseFnc_RangeCheck( ( float )eSoundCh, ( float )Tbl_SoundTable.E_SND_CH.E_SND_CH_LENGTH );

		//サウンドの一時停止
		auSndChList[( int )eSoundCh].Pause();
	}

	//----------------------------------------------------------------
	// Detail  ；サウンド一時停止解除
	// Argument：値
	// Return  ：none
	// Create  ：takada
	//----------------------------------------------------------------
	public void BaseFnc_SoundUnPause( Tbl_SoundTable.E_SND_CH eSoundCh )
	{
		Base_DataCheck.BaseFnc_RangeCheck( ( float )eSoundCh, ( float )Tbl_SoundTable.E_SND_CH.E_SND_CH_LENGTH );

		//サウンドの一時停止解除
		auSndChList[( int )eSoundCh].UnPause();
	}
}
