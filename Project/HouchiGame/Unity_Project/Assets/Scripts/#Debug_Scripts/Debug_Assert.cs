﻿using UnityEngine;

public static class Debug_Assert
{
	//----------------------------------------------------------------
	// Detail  ；アサート処理
	// Argument：条件判定式/表示文字
	// Return  ：none
	// Create  ：takada
	//----------------------------------------------------------------
	public static void DebugFnc_Assert(object arg_oLog1)
    {
        Debug.Assert(false, "Log1:" + arg_oLog1);
        //Debug.Break();
    }
    public static void DebugFnc_Assert(bool arg_bCond, object arg_oLog1)
    {
        Debug.Assert( arg_bCond,
            "Log1:" + arg_oLog1 + "\n"
            );
        //Debug.Break();
    }
    public static void DebugFnc_Assert(bool arg_bCond, object arg_oLog1, object arg_oLog2)
    {
        Debug.Assert( arg_bCond,
            "Log1:" + arg_oLog1 + "\n" +
            "Log2:" + arg_oLog2 + "\n"
            );
        //Debug.Break();
    }
    public static void DebugFnc_Assert(bool arg_bCond, object arg_oLog1, object arg_oLog2, object arg_oLog3)
    {
        Debug.Assert( arg_bCond,
            "Log1:" + arg_oLog1 + "\n" +
            "Log2:" + arg_oLog2 + "\n" +
            "Log3:" + arg_oLog3 + "\n"
            );
        //Debug.Break();
    }
    public static void DebugFnc_Assert(bool arg_bCond, object arg_oLog1, object arg_oLog2, object arg_oLog3, object arg_oLog4)
    {
        Debug.Assert( arg_bCond,
            "Log1:" + arg_oLog1 + "\n" +
            "Log2:" + arg_oLog2 + "\n" +
            "Log3:" + arg_oLog3 + "\n" +
            "Log4:" + arg_oLog4 + "\n"
            );
        //Debug.Break();
    }

	//----------------------------------------------------------------
	// Detail  ；コンソール文字表示
	// Argument：条件判定式/表示文字
	// Return  ：none
	// Create  ：takada
	//----------------------------------------------------------------
	public static void DebugFnc_Log(object arg_oLog1)
    {
        Debug.Log("Log1:" + arg_oLog1);
    }
    public static void DebugFnc_Log(object arg_oLog1, object arg_oLog2)
    {
        Debug.Log(
            "Log1:" + arg_oLog1 + "\n" +
            "Log2:" + arg_oLog2 + "\n"
            );
    }
    public static void DebugFnc_Log(object arg_oLog1, object arg_oLog2, object arg_oLog3)
    {
        Debug.Log(
            "Log1:" + arg_oLog1 + "\n" +
            "Log2:" + arg_oLog2 + "\n" +
            "Log3:" + arg_oLog3 + "\n"
            );
    }
    public static void DebugFnc_Log(object arg_oLog1, object arg_oLog2, object arg_oLog3, object arg_oLog4)
    {
        Debug.Log(
            "Log1:" + arg_oLog1 + "\n" +
            "Log2:" + arg_oLog2 + "\n" +
            "Log3:" + arg_oLog3 + "\n" +
            "Log4:" + arg_oLog4 + "\n"
            );
    }


	//----------------------------------------------------------------
	// Detail  ；強制停止
	// Argument：none
	// Return  ：none
	// Create  ：takada
	//----------------------------------------------------------------
	public static void DebugFnc_Break()
	{
		Debug.Break();
	}
}
