﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Man_Time : MonoBehaviour
{
	//タイマーの状態
	public enum E_TIMER_MODE
	{
		TIMER_MODE_TIMER	,
		TIMER_MODE_STOPWATCH,
		TIMER_MODE_CLOCK	,
		TIMER_MODE_LENGTH	,
	}

	//Sys_Timerのスクリプトリスト
	public Sys_Timer		csSysTimer;
	public Sys_StopWatch	csSysStopWatch;
	public Sys_Clock		csSysClock;

	public Obj_Timer		csObj_Timer;            //時間表示オブジェクト
	public Obj_TimerButton	csObj_TimerButton;       //ボタン切り替えオブジェクト

	private E_TIMER_MODE eTimerMode_state;          //タイマーのモード状態

	private void Reset()
	{
		//モード初期化
		eTimerMode_state = E_TIMER_MODE.TIMER_MODE_CLOCK;
		//対応するUIに変更
		csObj_TimerButton.Objfunc_UpdateButtonObjList( eTimerMode_state );
	}

	private void Start()
    {
		//代入チェック
		Base_DataCheck.BaseFnc_NullCheck( csSysTimer		);
		Base_DataCheck.BaseFnc_NullCheck( csSysStopWatch	);
		Base_DataCheck.BaseFnc_NullCheck( csSysClock		);
		Base_DataCheck.BaseFnc_NullCheck( csObj_Timer		);
		Base_DataCheck.BaseFnc_NullCheck( csObj_TimerButton );

		Reset();
	}

    private void Update()
    {
		switch( eTimerMode_state )
		{
			case E_TIMER_MODE.TIMER_MODE_TIMER:
				csSysTimer.SysFnc_Update();
				break;
			case E_TIMER_MODE.TIMER_MODE_STOPWATCH:
				csSysStopWatch.SysFnc_Update();
				break;
			case E_TIMER_MODE.TIMER_MODE_CLOCK:
				csSysClock.SysFnc_Update();
				break;
			default:
				Debug_Assert.DebugFnc_Assert( eTimerMode_state );
				eTimerMode_state = E_TIMER_MODE.TIMER_MODE_TIMER;
				csSysTimer.SysFnc_Update();
				break;
		}
	}
	
	//----------------------------------------------------------------
	// Detail  ；タイマータイプ変更
	// Argument：none
	// Return  ：none
	// Create  ：takada
	//----------------------------------------------------------------
	public void ManFunc_TimerTypeChange()
	{
		E_TIMER_MODE eTimerMode_TempState = eTimerMode_state;

		//次のモードへ
		eTimerMode_TempState++;

		//最大になった場合初期値へ
		if( E_TIMER_MODE.TIMER_MODE_LENGTH <= eTimerMode_TempState )
		{
			eTimerMode_TempState = E_TIMER_MODE.TIMER_MODE_TIMER;
		}

		//範囲チェック
		eTimerMode_state = ( E_TIMER_MODE )Base_DataCheck.BaseFnc_RangeCheck( ( int )eTimerMode_TempState, ( int )E_TIMER_MODE.TIMER_MODE_TIMER, ( int )E_TIMER_MODE.TIMER_MODE_LENGTH );

		//対応するUIに変更
		csObj_TimerButton.Objfunc_UpdateButtonObjList( eTimerMode_state );

		//システム初期化
		csSysTimer.SysFunc_Reset();
		csSysStopWatch.SysFunc_Reset();
		csSysClock.SysFunc_Reset();
	}
}
