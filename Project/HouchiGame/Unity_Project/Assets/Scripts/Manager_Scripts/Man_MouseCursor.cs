﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Man_MouseCursor : MonoBehaviour
{
	public Camera caCamera;					//カメラオブジェクト
	public ParticleSystem psParticleSystem;	//パーティクルシステム


	private void Reset()
	{
		//メインカメラを取得
		caCamera = Camera.main;
	}

	private void Start()
    {
		Reset();

		//代入チェック
		Base_DataCheck.BaseFnc_NullCheck( caCamera );
		Base_DataCheck.BaseFnc_NullCheck( psParticleSystem );

	}

	private void Update()
    {
		//マウス座標を取得
		Vector3 veMousePos = Input.mousePosition;
		//奥行を補正
		veMousePos.z = 10f;
		//座標更新
		transform.position = caCamera.ScreenToWorldPoint( veMousePos );

		//マウスが押下された瞬間
		if( Input.GetMouseButtonDown( 0 )) 
		{
			//エフェクト再生
			psParticleSystem.Play();
		}
	}
}
