using UnityEngine;

public class Tbl_SoundTable : MonoBehaviour
{

	//サウンド配列要素クラス
	protected class cSndListStat
	{
		private string      sFilePath   ;   //ファイルパス
		private int         iChannel    ;   //チャンネル
		private int         iVolume     ;   //ボリューム
		private bool        bLoop       ;   //ループ
		private bool        bKeep       ;   //保持

		public cSndListStat( string sArg1, int iArg2, int iArg3, bool bArg4, bool bArg5 )
		{
			sFilePath = sArg1;
			iChannel = iArg2;
			iVolume = iArg3;
			bLoop = bArg4;
			bKeep = bArg5;
		}

		public string cmGetFilePash() { return sFilePath; }
		public int cmGetChannel() { return iChannel; }
		public int cmGetVolume() { return iVolume; }
		public bool cmGetLoop() { return bLoop; }
		public bool cmGetKeep() { return bKeep; }
	}

	//サウンドチャンネル
	public enum E_SND_CH
	{
		E_CH_SIS_00	=0	,//システム音
		E_CH_SE_10	=10	,//SE
		E_CH_SE_11	=11	,//SE
		E_CH_SE_12	=12	,//SE
		E_CH_SE_13	=13	,//SE
		E_CH_SE_14	=14	,//
		E_CH_SE_15	=15	,//
		E_CH_SE_16	=16	,//
		E_CH_SE_17	=17	,//
		E_CH_SE_18	=18	,//
		E_CH_SE_19	=19	,//
		E_CH_VOICE_20	=20	,//ボイス
		E_CH_VOICE_21	=21	,//ボイス
		E_CH_VOICE_22	=22	,//
		E_CH_VOICE_23	=23	,//
		E_CH_VOICE_24	=24	,//
		E_CH_VOICE_25	=25	,//
		E_CH_VOICE_26	=26	,//
		E_CH_VOICE_27	=27	,//
		E_CH_VOICE_28	=28	,//
		E_CH_VOICE_29	=29	,//
		E_CH_BGM_30	=30	,//BGM
		E_CH_BGM_31	=31	,//BGM
		E_CH_BGM_32	=32	,//
		E_CH_BGM_33	=33	,//
		E_CH_BGM_34	=34	,//
		E_CH_BGM_35	=35	,//
		E_CH_BGM_36	=36	,//
		E_CH_BGM_37	=37	,//
		E_CH_BGM_38	=38	,//
		E_CH_BGM_39	=39	,//
		E_SND_CH_LENGTH	,
	}



	//サウンド配列要素
	public enum E_SND_TAG
	{
		E_SND_1000_SE_1				,		//テスト用SE1
		E_SND_1001_SE_2				,		//テスト用SE2
		E_SND_1100_SE_3				,		//テスト用SE3
		E_SND_1200_SE_4				,		//テスト用SE4
		E_SND_2000_VO_1				,		//テスト用VO1
		E_SND_2001_VO_2				,		//テスト用VO2
		E_SND_2100_VO_3				,		//テスト用VO3
		E_SND_2101_VO_4				,		//テスト用VO4
		E_SND_3000_BGM_1				,		//テスト用BGM1
		E_SND_3001_BGM_2				,		//テスト用BGM2
		E_SND_3100_BGM_3				,		//テスト用BGM3
		E_SND_3101_BGM_4				,		//テスト用BGM4
		E_SND_LIST_DATA_TAG_LENGTH	,
	}

	//サウンドデータ配列
	protected cSndListStat[] T_SND_DATA_LIST = new cSndListStat[(int)E_SND_TAG.E_SND_LIST_DATA_TAG_LENGTH]
	{ 
		new cSndListStat("Sounds/Se/1000_Se"	,10	,10	,false	,false	)	,			//テスト用SE1
		new cSndListStat("Sounds/Se/1001_Se"	,10	,10	,false	,false	)	,			//テスト用SE2
		new cSndListStat("Sounds/Se/1100_Se"	,11	,10	,false	,false	)	,			//テスト用SE3
		new cSndListStat("Sounds/Se/1200_Se"	,12	,10	,false	,false	)	,			//テスト用SE4
		new cSndListStat("Sounds/Vo/2000_Vo"	,20	,10	,false	,false	)	,			//テスト用VO1
		new cSndListStat("Sounds/Vo/2001_Vo"	,20	,10	,false	,false	)	,			//テスト用VO2
		new cSndListStat("Sounds/Vo/2100_Vo"	,21	,10	,false	,false	)	,			//テスト用VO3
		new cSndListStat("Sounds/Vo/2101_Vo"	,21	,10	,false	,false	)	,			//テスト用VO4
		new cSndListStat("Sounds/Bgm/3000_Bgm"	,30	,10	,true	,true	)	,			//テスト用BGM1
		new cSndListStat("Sounds/Bgm/3001_Bgm"	,30	,10	,true	,true	)	,			//テスト用BGM2
		new cSndListStat("Sounds/Bgm/3100_Bgm"	,31	,10	,true	,true	)	,			//テスト用BGM3
		new cSndListStat("Sounds/Bgm/3101_Bgm"	,31	,10	,true	,true	)	,			//テスト用BGM4
	};

}
